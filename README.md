#  Lab : Utilisation des persistentVolumes pour monter du stockage persistant sur des conteneurs

------------

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

# Objectifs

1. Créer un volume persistant qui permet l'expansion des revendications

2. Créer une réclamation de volume persistant

3. Créer un pod qui utilise un PersistentVolume pour le stockage


# Contexte 

Votre entreprise, BeeHive, développe des applications pour Kubernetes. Ils anticipent des besoins de stockage de plus en plus complexes à l'avenir et veulent donc s'assurer qu'ils peuvent exploiter tout le potentiel du stockage Kubernetes avec PersistentVolumes.

Votre tâche consiste à créer une application qui utilise un PersistentVolume pour le stockage. Assurez-vous que le volume de l'application peut être étendu au cas où ses besoins de stockage pourraient augmenter ultérieurement.

>![Alt text](img/image.png)



### Introduction
Les PersistentVolumes (PV) offrent un moyen de traiter le stockage comme une ressource dynamique dans Kubernetes. Ce laboratoire vous permettra de démontrer vos connaissances sur les PersistentVolumes. Vous allez monter un stockage persistant sur un conteneur en utilisant un PersistentVolume et un PersistentVolumeClaim.


# Application 

#### Étape 1 : Connexion au Serveur de Laboratoire

Connectez-vous au serveur à l'aide des informations d'identification fournies :

```sh
ssh -i id_rsa user@<PUBLIC_IP_ADDRESS>
```

#### Étape 2 : Créer un persistentVolume qui permet l'extension des requêtes

1. Créez une Storage Class personnalisée :

```sh
nano localdisk.yml
```

2. Définissez la Storage Class :

```yaml
apiVersion: storage.k8s.io/v1 
kind: StorageClass 
metadata:
  name: localdisk 
provisioner: kubernetes.io/no-provisioner
allowVolumeExpansion: true
```

3. Enregistrez et quittez le fichier 

4. Créez la Storage Class :

```sh
kubectl create -f localdisk.yml
```

>![Alt text](img/image-1.png)
*StorageClass crée*

5. Créez le PersistentVolume :

```sh
nano host-pv.yml
```

6. Définissez le PersistentVolume avec une taille de 1Gi :

```yaml
kind: PersistentVolume 
apiVersion: v1 
metadata: 
  name: host-pv 
spec: 
  storageClassName: localdisk
  persistentVolumeReclaimPolicy: Recycle 
  capacity: 
    storage: 1Gi 
  accessModes: 
    - ReadWriteOnce 
  hostPath: 
    path: /var/output
```

7. Enregistrez et quittez le fichier en appuyant sur la touche `ESC` et en utilisant `:wq`.

8. Créez le PersistentVolume :

```sh
kubectl create -f host-pv.yml
```

>![Alt text](img/image-2.png)
*PV crée*

9. Vérifiez le statut du PersistentVolume :

```sh
kubectl get pv
```

>![Alt text](img/image-3.png)
*Liste de pv*

#### Étape 3 : Créer un PersistentVolumeClaim

1. Créez un PersistentVolumeClaim :

```sh
nano host-pvc.yml
```

2. Définissez le PersistentVolumeClaim avec une taille de 100Mi :

```yaml
apiVersion: v1 
kind: PersistentVolumeClaim 
metadata: 
  name: host-pvc 
spec: 
  storageClassName: localdisk 
  accessModes: 
    - ReadWriteOnce 
  resources: 
    requests: 
      storage: 100Mi
```

3. Enregistrez et quittez le fichier en appuyant sur la touche `ESC` et en utilisant `:wq`.

4. Créez le PersistentVolumeClaim :

```sh
kubectl create -f host-pvc.yml
```

>![Alt text](img/image-4.png)
*PVC crée*

5. Vérifiez le statut du PersistentVolume et du PersistentVolumeClaim pour s'assurer qu'ils sont liés :

```sh
kubectl get pv
kubectl get pvc
```

>![Alt text](img/image-5.png)
*Liste de PV et PVC actif*

#### Étape 4 : Créer un pod qui utilise un persistentVolume pour le stockage

1. Créez un Pod qui utilise le PersistentVolumeClaim :

```sh
nano pv-pod.yml
```

2. Définissez le Pod :

```yaml
apiVersion: v1 
kind: Pod 
metadata: 
  name: pv-pod 
spec: 
  containers: 
  - name: busybox 
    image: busybox 
    command: ['sh', '-c', 'while true; do echo Success! > /output/success.txt; sleep 5; done'] 
```

3. Montez le PersistentVolume à l'emplacement `/output` en ajoutant les lignes suivantes sous la spécification `volumes` au même niveau que les spécifications des conteneurs :

```yaml
    volumes: 
    - name: pv-storage 
        persistentVolumeClaim: 
            claimName: host-pvc
```

4. Dans la spécification des conteneurs, en dessous de `command`, définissez la liste des montages de volumes :

```yaml
        volumeMounts: 
        - name: pv-storage 
            mountPath: /output 
```

5. Assurez-vous que le fichier final `pv-pod.yml` ressemble à ceci :

```yaml
apiVersion: v1 
kind: Pod 
metadata: 
  name: pv-pod 
spec: 
  containers: 
  - name: busybox 
    image: busybox 
    command: ['sh', '-c', 'while true; do echo Success! > /output/success.txt; sleep 5; done'] 
    volumeMounts: 
    - name: pv-storage 
      mountPath: /output 
  volumes: 
  - name: pv-storage 
    persistentVolumeClaim: 
      claimName: host-pvc
```

6. Enregistrez et quittez le fichier en appuyant sur la touche `ESC` et en utilisant `:wq`.

7. Créez le Pod :

```sh
kubectl create -f pv-pod.yml
```

>![Alt text](img/image-6.png)
*Pod crée*


8. Vérifiez que le Pod est opérationnel :

```sh
kubectl get pods
```

>![Alt text](img/image-7.png)

9. Si vous le souhaitez, vous pouvez vous connecter au nœud de travail et vérifier les données de sortie :

```sh
ssh -i id_rsa user@PUBLIC_IP_ADDRESS_WORKER_NODE

cat /var/output/success.txt
```

>![Alt text](img/image-8.png)

Ce laboratoire vous guide à travers le processus de création et d'utilisation de PersistentVolumes et PersistentVolumeClaims pour monter du stockage persistant sur des conteneurs Kubernetes.
